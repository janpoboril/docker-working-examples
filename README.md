# Working examples of my Docker stacks

This templates are based on stacks what I used in real projects. They are all proofed to work in their time. But because of rapid development od Docker, related tools and my knowledge, some of them could be suboptimal today, so I recommend to prefer newer stacks (see GIT history there).

If you find something not working or suboptimal feel free to file an issue or send pull request.

## [gitlab-ci-with-docker-machine](gitlab-ci-with-docker-machine)

- Deploy target for CI is Docker Machine (one permanent server accessible by SSH)
- Drupal 7 with PHP 5.6, Drush and MySQL 5
- Automatic import of DB with schema and example data
- Init script waiting for available DB and then triggers other scripts like DB migration
- Conditional Drupal configuration by environment varibales
- Compose files for local development, CI and production with overriding
- GitLab CI configuration builds web image, deploys [review apps](https://docs.gitlab.com/ee/ci/review_apps/) (all branches) and manually production (master only)

## [gitlab-runner](gitlab-runner)

- GitLab Runner in Docker
- Docker server available for jobs (`docker build` works)
- GitLab jobs cache to S3 (expecting running on EC2 instance having access to the bucket)

# .bash_profile tweeks

Useful functions and prompt customizations what helps me in my everyday work.

- Customize bash prompt to print current GIT branch and Docker Machine name.
- `dmenv`: Set env variables for Docker Machine
- `dstats`: Dockers stats with container names
- `dsh`: Open shell into app or php service of current Docker Compose project
- `uli`: Get login link for Drupal superuser without entering container

![how it works](https://user-images.githubusercontent.com/614232/34212178-c9bb110e-e59b-11e7-82c6-d9c875cc221c.gif)

[see my .bash_profile file](https://gist.github.com/iBobik/ba6eb36cb959b7ce88f50298069ec39f)
